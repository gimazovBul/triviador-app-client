package ru.itis.client;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(separators = "=")
public class Args {
    @Parameter(names = {"--server-port"})
    private
    Integer port;
    @Parameter(names = {"--server-ip"})
    private
    String serverIp;

    public Integer getPort() {
        return port;
    }

    public String getServerIp() {
        return serverIp;
    }
}
