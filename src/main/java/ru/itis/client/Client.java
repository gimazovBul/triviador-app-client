package ru.itis.client;


import ru.itis.clientServices.impl.ResponseDispatcherImpl;
import ru.itis.clientServices.interfaces.ResponseDispatcher;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Client {
    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;
    private ResponseDispatcher responseDispatcher;

    public void startConnection(String ip, int port) {
        try {
            clientSocket = new Socket(ip, port);
            out = new PrintWriter(clientSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            new Thread(receiverMessagesTask).start();
            responseDispatcher = new ResponseDispatcherImpl();
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    public void sendMessage(String message) {
        System.out.println("Sending message...");
        System.out.println(message);
        out.println(message);
    }

    private Runnable receiverMessagesTask = new Runnable() {
        @Override
        public void run() {
            while (true) {
                try {
                    String responseLine = in.readLine();
                    if (responseLine != null) {
                        System.out.println("Client: got response " + responseLine);
                        responseDispatcher.resolve(responseLine);
                    }
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
            }
        }
    };
}
