package ru.itis.client;

import com.beust.jcommander.JCommander;

public class ConnectionStarter {
    private Args argsClientClass;

    public ConnectionStarter(String[] args) {
        argsClientClass = new Args();
        JCommander.newBuilder()
                .addObject(argsClientClass)
                .args(args)
                .build();
    }
    public void startClient(Client client) {
        client.startConnection(
                argsClientClass.getServerIp(), argsClientClass.getPort()
        );
    }
}
