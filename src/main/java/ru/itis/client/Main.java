package ru.itis.client;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import ru.itis.clientSideWrappers.UserWrapper;
import ru.itis.dto.UserDto;

import java.util.UUID;

import static ru.itis.clientConfig.ClientContext.get;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        UserDto userDto = new UserDto();
        userDto.setId(UUID.randomUUID().toString());
        UserWrapper.set(userDto);
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/home.fxml"));
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();

    }

    public static void main(String[] args) {
        ConnectionStarter starter = new ConnectionStarter(args);
        starter.startClient(get().getClient());
        launch(args);
    }
}
