package ru.itis.clientConfig;


import lombok.Data;
import ru.itis.client.Client;
import ru.itis.clientServices.impl.*;
import ru.itis.clientServices.interfaces.*;

@Data
public class ClientContext {
    private Client client;
    private static ClientContext clientContext;
    private HomeService homeService;
    private RoomService roomService;
    private FailService failService;
    private QuestionService questionService;
    private ResultService resultService;

    public static ClientContext get() {
        if (clientContext == null) {
            clientContext = new ClientContext();
        }
        return clientContext;
    }

    private ClientContext() {
        client = new Client();
        homeService = new HomeServiceImpl();
        roomService = new RoomServiceImpl();
        resultService = new ResultServiceImpl();
        failService = new FailServiceImpl();
        questionService = new QuestionServiceImpl();
    }

}
