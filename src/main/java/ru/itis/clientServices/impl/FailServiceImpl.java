package ru.itis.clientServices.impl;

import lombok.Data;
import ru.itis.clientServices.interfaces.FailService;

@Data
public class FailServiceImpl implements FailService {

    private volatile boolean exists;
    private volatile String error;

    @Override
    public void setError(String error) {
        this.error = error;
        exists = true;
    }

    @Override
    public String getError() {
        return error;
    }
}
