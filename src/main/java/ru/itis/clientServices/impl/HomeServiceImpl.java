package ru.itis.clientServices.impl;


import ru.itis.clientServices.interfaces.HomeService;
import ru.itis.clientSideWrappers.UserWrapper;
import ru.itis.dto.BaseDto;
import ru.itis.dto.CodeDto;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;

public class HomeServiceImpl implements HomeService {

    public String generateCode() {
        return randomAlphanumeric(6);
    }

    @Override
    public BaseDto prepareCodeDto(String code) throws IllegalStateException{
        if (code != null && !"".equals(code)) {
            CodeDto codeDto = new CodeDto(code);
            codeDto.setUserId(UserWrapper.get().getId());
            return codeDto;
        }
        throw new IllegalStateException();
    }
}
