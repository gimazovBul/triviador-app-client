package ru.itis.clientServices.impl;


import ru.itis.clientServices.interfaces.QuestionService;
import ru.itis.clientSideWrappers.UserWrapper;
import ru.itis.dto.AnswerDto;
import ru.itis.dto.QuestionDto;
import ru.itis.dto.QuestionRequestDto;

public class QuestionServiceImpl implements QuestionService {
    private volatile boolean ready;
    private volatile QuestionDto questionDto;

    @Override
    public QuestionRequestDto prepareQuestReq() {
        return new QuestionRequestDto();
    }

    @Override
    public void setQuestion(QuestionDto question){
        this.questionDto = question;
        ready = true;
    }

    @Override
    public QuestionDto getQuestion() {
        return questionDto;
    }

    @Override
    public boolean isReady() {
        return ready;
    }

    public void setReady(boolean ready) {
        this.ready = ready;
    }

    @Override
    public AnswerDto sendAnswer(int answerId) {
        return new AnswerDto(UserWrapper.get().getId(), answerId);
    }


}
