package ru.itis.clientServices.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.itis.clientConfig.ClientContext;
import ru.itis.clientServices.interfaces.*;
import ru.itis.dto.FailDto;
import ru.itis.dto.QuestionDto;
import ru.itis.dto.ResultDto;
import ru.itis.dto.RoomDto;
import ru.itis.protocol.Response;

public class ResponseDispatcherImpl implements ResponseDispatcher {
    private ObjectMapper mapper;
    private RoomService roomService;
    private FailService failService;
    private QuestionService questionService;
    private ClientContext clientContext;
    private ResultService resultService;

    public ResponseDispatcherImpl() {
        mapper = new ObjectMapper();
        clientContext = ClientContext.get();
        roomService = clientContext.getRoomService();
        failService = clientContext.getFailService();
        resultService = clientContext.getResultService();
        questionService = clientContext.getQuestionService();

    }

    public synchronized void resolve(String message) throws JsonProcessingException {
        Response response = mapper.readValue(message, Response.class);
        switch (response.getHeader()) {
            case "sucJoin": {
                RoomDto roomDto = (RoomDto) response.getData();
                roomService.setRoom(roomDto);
                break;
            }
            case "fail": {
                FailDto failDto = (FailDto) response.getData();
                System.out.println("ResponseDispatcher " + failDto.getMessage());
                failService.setError(failDto.getMessage());
                break;
            }
            case "quesResp": {
                QuestionDto questionDto = (QuestionDto) response.getData();
                questionService.setQuestion(questionDto);
                break;
            }
            case "res" : {
                ResultDto resultDto = (ResultDto) response.getData();
                resultService.setResults(resultDto);
                break;
            }
        }
    }
}
