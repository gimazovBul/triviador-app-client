package ru.itis.clientServices.impl;


import ru.itis.clientServices.interfaces.ResultService;
import ru.itis.dto.ResultDto;

public class ResultServiceImpl implements ResultService {
    private volatile boolean ready;
    private volatile ResultDto results;

    @Override
    public void setResults(ResultDto results) {
        System.out.println("setting results");
        this.results = results;
        ready = true;
    }

    @Override
    public ResultDto getResults() {
        System.out.println("got results ");
        setReady(false);
        return results;
    }

    @Override
    public boolean isReady() {
        return ready;
    }

    @Override
    public void setReady(boolean ready) {
        this.ready = ready;
    }
}
