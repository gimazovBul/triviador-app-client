package ru.itis.clientServices.impl;


import ru.itis.clientServices.interfaces.RoomService;
import ru.itis.dto.RoomDto;

public class RoomServiceImpl implements RoomService {
    private volatile RoomDto roomDto;
    private volatile boolean received;

    @Override
    public void setRoom(RoomDto roomDto) {
        System.out.println("setting roomDto...");
        this.roomDto = roomDto;
        received = true;
        System.out.println(this.roomDto);
    }

    public boolean isReceived() {
        return received;
    }

    @Override
    public synchronized RoomDto getRoom() {
        return roomDto;
    }
}
