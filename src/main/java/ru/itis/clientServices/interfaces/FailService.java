package ru.itis.clientServices.interfaces;

public interface FailService {
    void setError(String error);
    String getError();
    boolean isExists();
    void setExists(boolean exists);

}
