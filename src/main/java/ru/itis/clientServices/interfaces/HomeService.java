package ru.itis.clientServices.interfaces;


import ru.itis.dto.BaseDto;

public interface HomeService {
    String generateCode();
    BaseDto prepareCodeDto(String code) throws IllegalStateException;
}
