package ru.itis.clientServices.interfaces;


import ru.itis.dto.AnswerDto;
import ru.itis.dto.QuestionDto;
import ru.itis.dto.QuestionRequestDto;

public interface QuestionService {
    QuestionRequestDto prepareQuestReq();
    QuestionDto getQuestion();
    void setQuestion(QuestionDto question);
    boolean isReady();
    void setReady(boolean ready);
    AnswerDto sendAnswer(int answerId);
}
