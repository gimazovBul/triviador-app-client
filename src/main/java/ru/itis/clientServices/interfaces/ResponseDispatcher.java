package ru.itis.clientServices.interfaces;

import com.fasterxml.jackson.core.JsonProcessingException;

public interface ResponseDispatcher {
    void resolve(String message) throws JsonProcessingException;
}
