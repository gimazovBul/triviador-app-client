package ru.itis.clientServices.interfaces;


import ru.itis.dto.ResultDto;

public interface ResultService {
    void setResults(ResultDto results);

    ResultDto getResults();

    boolean isReady();

    void setReady(boolean ready);
}
