package ru.itis.clientServices.interfaces;


import ru.itis.dto.RoomDto;

public interface RoomService {
    void setRoom(RoomDto roomDto);
    boolean isReceived();
    RoomDto getRoom();
}
