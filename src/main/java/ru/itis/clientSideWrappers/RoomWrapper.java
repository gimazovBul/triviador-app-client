package ru.itis.clientSideWrappers;


import ru.itis.dto.RoomDto;

public class RoomWrapper {
    private final static ThreadLocal<RoomDto> threadLocal = new ThreadLocal<>();

    public static void set(RoomDto roomDto){
        threadLocal.set(roomDto);
    }

    public static RoomDto get(){
        return threadLocal.get();
    }
}
