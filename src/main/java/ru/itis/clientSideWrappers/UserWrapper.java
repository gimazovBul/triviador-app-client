package ru.itis.clientSideWrappers;


import ru.itis.dto.UserDto;

public class UserWrapper {
    private final static ThreadLocal<UserDto> threadLocal = new ThreadLocal<>();

    public static void set(UserDto userDto){
        threadLocal.set(userDto);
    }

    public static UserDto get(){
        return threadLocal.get();
    }
}
