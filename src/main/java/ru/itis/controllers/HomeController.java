package ru.itis.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;
import lombok.Data;
import ru.itis.client.Client;
import ru.itis.clientConfig.ClientContext;
import ru.itis.clientServices.impl.HomeServiceImpl;
import ru.itis.clientServices.interfaces.FailService;
import ru.itis.clientServices.interfaces.HomeService;
import ru.itis.clientServices.interfaces.RoomService;
import ru.itis.dto.CodeDto;
import ru.itis.protocol.Request;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

@Data
public class HomeController implements Initializable {
    @FXML
    public Button generateButton;
    @FXML
    public Label generatedCode;
    @FXML
    private TextField code;
    @FXML
    private Button goButton;
    @FXML
    private Label error;
    @FXML
    private Label waiting;

    private ClientContext clientContext;
    private Client client;
    private HomeService homeService;
    private ObjectMapper mapper;
    private FailService failService;
    private RespWaiter respWaiter;
    private ActionEvent event;
    private volatile boolean ready;

    public HomeController() {
        clientContext = ClientContext.get();
        homeService = new HomeServiceImpl();
        client = clientContext.getClient();
        mapper = new ObjectMapper();
        failService = clientContext.getFailService();
        respWaiter = new RespWaiter(this);
        respWaiter.start();
    }

    @FXML
    public void sendCode(ActionEvent event) {
        this.event = event;
        waiting.setVisible(false);
        generateButton.setDisable(true);
        if (!"".equals(code.getText()) && code.getText() != null) {
            Request<CodeDto> request = Request.build("code", (CodeDto) homeService.prepareCodeDto(code.getText()));
            code.setDisable(true);
            try {
                goButton.setDisable(false);
                failService.setExists(false);
                client.sendMessage(mapper.writeValueAsString(request));
                goButton.setDisable(true);
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
            waiting.setVisible(true);
        } else {
            error.setVisible(true);
            error.setTextFill(Paint.valueOf("#CF0005"));
            error.setText("code can't be null");
        }
    }

    public void redirect(ActionEvent event, boolean failExists) throws IOException {
        waiting.setVisible(false);
        if (failExists) {
            goButton.setDisable(false);
            System.out.println("error occurred room is full");
            error.setVisible(true);
            error.setTextFill(Paint.valueOf("#CF0005"));
            error.setText(failService.getError());
            code.setDisable(false);
            respWaiter.interrupt();
        } else {
            System.out.println("no errors ");
            System.out.println("redirecting...");
            Parent parent = FXMLLoader.load(getClass().getResource("/fxml/room.fxml"));
            Scene newPageScene = new Scene(parent);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(newPageScene);
            appStage.show();
        }
    }

    @FXML
    public void generateCode() {
        generatedCode.setVisible(true);
        generatedCode.setText(homeService.generateCode());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
       code.textProperty().bindBidirectional(generatedCode.textProperty());
    }

    @Data
    private class RespWaiter extends Thread {
        private RoomService roomService;
        private FailService failService;
        private HomeController homeController;
        private final ClientContext clientContext = ClientContext.get();
        private volatile String waitingText;
        private volatile boolean ready;

        public RespWaiter(HomeController homeController) {
            roomService = clientContext.getRoomService();
            failService = clientContext.getFailService();
            this.homeController = homeController;
        }

        public synchronized String getWaitingText() {
            System.out.println(waitingText);
            return waitingText;
        }

        public void setWaitingText(String waitingText) {
            this.waitingText = waitingText;
        }

        @Override
        public void run() {
            setWaitingText("waiting...");
            while (!roomService.isReceived() && !failService.isExists()) {}
            ready = true;
            Runnable runnable = () ->
            {
                try {
                    homeController.redirect(homeController.getEvent(), failService.isExists());
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
            };

            if (ready && Platform.isFxApplicationThread()) {
                new Thread(runnable).start();
            } else Platform.runLater(runnable);
        }
    }
}


