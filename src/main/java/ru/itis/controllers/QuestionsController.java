package ru.itis.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.util.concurrent.AtomicDouble;
import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;
import lombok.Data;
import lombok.SneakyThrows;
import ru.itis.client.Client;
import ru.itis.clientConfig.ClientContext;
import ru.itis.clientServices.interfaces.QuestionService;
import ru.itis.clientServices.interfaces.ResultService;
import ru.itis.dto.AnswerDto;
import ru.itis.dto.QuestionDto;
import ru.itis.protocol.Request;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;


@Data
public class QuestionsController implements Initializable {
    @FXML
    public Button first;
    @FXML
    public Button third;
    @FXML
    public Button second;
    @FXML
    public Label text;
    @FXML
    public Label timer;

    private QuestionService questionService;
    private ResultService resultService;
    private List<Button> buttons;
    private Client client;
    private ObjectMapper mapper;
    private volatile AtomicDouble opacity = new AtomicDouble();
    private volatile boolean answered;
    private ResultWaiter resultWaiter;
    private volatile boolean timeIsUp;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        System.out.println("-----------------------------");
        System.out.println("initializing questionController...");
        buttons = Arrays.asList(first, second, third);
        ClientContext clientContext = ClientContext.get();
        opacity.set(800D);
        questionService = clientContext.getQuestionService();
        resultService = clientContext.getResultService();
        resultService.setReady(false);
        client = clientContext.getClient();
        mapper = new ObjectMapper();
        resultWaiter = new ResultWaiter(this);
        resultWaiter.setDaemon(true);
        prepareQuestion();
        AnimationTimer animationTimer = new Timer();
        animationTimer.start();
        resultWaiter.start();
    }

    private void prepareQuestion() {
        QuestionDto questionDto =
                questionService.getQuestion();
        text.setText(questionDto.getText());
        first.setText(questionDto.getAnswers()[0]);
        second.setText(questionDto.getAnswers()[1]);
        third.setText(questionDto.getAnswers()[2]);
    }

    //if time is up
    public void answer() throws JsonProcessingException {
        AnswerDto answerDto = questionService.sendAnswer(-1);
        Request<AnswerDto> request = Request.build("answer", answerDto);
        client.sendMessage(mapper.writeValueAsString(request));
    }

    @FXML
    public void answer(ActionEvent event) throws JsonProcessingException {
        timer.setVisible(false);
        first.setDisable(true);
        second.setDisable(true);
        third.setDisable(true);
        Button answer = (Button) event.getSource();
        System.out.println("------------------");
        System.out.println(answer.getId());
        System.out.println(buttons.indexOf(answer));
        System.out.println(questionService.getQuestion().getRightInd());
        System.out.println("------------------");

        int answerId;
        if (answer.getId().equals("first")) answerId = 0;
        else if (answer.getId().equals("second")) answerId = 1;
        else answerId = 2;

        buttons.get(questionService.getQuestion().getRightInd()).setTextFill(Paint.valueOf("#0a9c42"));
        if (questionService.getQuestion().getRightInd() == answerId) {
            System.out.println("user is right");
            buttons.stream()
                    .filter(b -> buttons.indexOf(b) != answerId).forEach(button -> button.setVisible(false));
        } else {
            buttons.get(answerId).setTextFill(Paint.valueOf("#CF0005"));
            buttons.stream()
                    .filter(button -> buttons.indexOf(button) != answerId
                            && buttons.indexOf(button) != questionService.getQuestion().getRightInd())
                    .findFirst().get().setVisible(false);
        }

        AnswerDto answerDto = questionService.sendAnswer(answerId);
        Request<AnswerDto> request = Request.build("answer", answerDto);
        client.sendMessage(mapper.writeValueAsString(request));
        answered = true;

    }

    public void redirect() throws IOException {
        System.out.println("redirecting to ResultController...");
        System.out.println("timeIsUp " + timeIsUp);
        Parent parent = FXMLLoader.load(getClass().getResource("/fxml/results.fxml"));
        Stage stage = (Stage) first.getScene().getWindow();
        Scene scene = new Scene(parent);
        stage.setScene(scene);
        stage.show();
    }


    private class Timer extends AnimationTimer {

        @Override
        public void handle(long now) {
            opacity.set(opacity.get() - 1.5);
            timer.setText(String.valueOf(opacity.intValue() / 100));
            if (opacity.get() <= 0) {
                timeIsUp = true;
                if (!answered) {
                    changeUi();
                    try {
                        answer();
                    } catch (JsonProcessingException e) {
                        throw new IllegalStateException(e);
                    }
                }
                System.out.println("Animation stopped");
                timer.setVisible(false);
                stop();
            }
        }

        private void changeUi() {
            buttons.get(questionService.getQuestion().getRightInd()).setTextFill(Paint.valueOf("#0a9c42"));
            buttons.stream()
                    .filter(b -> buttons.indexOf(b) != questionService.getQuestion().getRightInd())
                    .forEach(button -> button.setTextFill(Paint.valueOf("#CF0005")));
            first.setDisable(true);
            second.setDisable(true);
            third.setDisable(true);
            timer.setVisible(false);
        }
    }

    private class ResultWaiter extends Thread {
        private QuestionsController questionsController;

        public ResultWaiter(QuestionsController questionsController) {
            this.questionsController = questionsController;
        }

        @SneakyThrows
        @Override
        public void run() {
            while (!resultService.isReady()) {
            }
            System.out.println("resultService.isReady() " + resultService.isReady());
            resultService.setReady(false);

            Runnable runnable = () ->
            {
                try {
                    questionsController.redirect();
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
            };
            if (Platform.isFxApplicationThread()) new Thread(runnable).start();
            else Platform.runLater(runnable);
        }

    }
}