package ru.itis.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.util.concurrent.AtomicDouble;
import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;
import lombok.SneakyThrows;
import ru.itis.clientServices.interfaces.QuestionService;
import ru.itis.clientServices.interfaces.ResultService;
import ru.itis.clientSideWrappers.RoomWrapper;
import ru.itis.clientSideWrappers.UserWrapper;
import ru.itis.dto.QuestionRequestDto;
import ru.itis.dto.ResultDto;
import ru.itis.dto.UserDto;
import ru.itis.protocol.Request;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

import static ru.itis.clientConfig.ClientContext.get;


public class ResultsController implements Initializable {
    @FXML
    public Label player1;
    @FXML
    public Label player2;
    @FXML
    public Label player3;
    @FXML
    public Label res1;
    @FXML
    public Label res2;
    @FXML
    public Label res3;
    @FXML
    public Label fin;
    @FXML
    public Button again;

    private List<Label> playerLabels;
    private List<Label> resLabels;
    private ResultService resultService;
    private volatile boolean timeIsUp;
    private volatile boolean cont; //game continues
    private QuestionWaiter questionWaiter;
    private QuestionService questionService;
    private ObjectMapper mapper;
    private AtomicDouble opacity;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        System.out.println("initializing resultsController...");
        opacity = new AtomicDouble(200D);
        questionWaiter = new QuestionWaiter(this);
        playerLabels = Arrays.asList(player1, player2, player3);
        resLabels = Arrays.asList(res1, res2, res3);
        resultService = get().getResultService();
        questionService = get().getQuestionService();
        mapper = new ObjectMapper();
        prepareResults();
        questionWaiter.setDaemon(true);
    }

    private void prepareResults() {
        System.out.println("preparing results...");
        ResultDto resultDto = resultService.getResults();
        System.out.println("resultDto.getType() " + resultDto.getType());
        if (resultDto.getType().equals(ResultDto.Type.CONT)) {
            cont = true;
        } else if (resultDto.getType().equals(ResultDto.Type.END)) {
//            again.setVisible(true);
            fin.setText("FINISH");
        }
        for (UserDto user : resultDto.getUsers()) {
            Label playerLabel = playerLabels.get(RoomWrapper.get().getPlayers().indexOf(user));
            Label resLabel = resLabels.get(RoomWrapper.get().getPlayers().indexOf(user));
            playerLabel.setTextFill(Paint.valueOf(user.getColor()));
            resLabel.setText(String.valueOf(user.getPointsAmount()));
        }
        new Timer().start();

        try {
            if (UserWrapper.get().isHost()) sendReq();
        } catch (JsonProcessingException e) {
            throw new IllegalStateException(e);
        }
    }

    public void redirect() throws IOException {
        System.out.println("redirecting to questionController...");
        System.out.println("timeIsUp " + timeIsUp);
        Parent parent = FXMLLoader.load(getClass().getResource("/fxml/questions.fxml"));
        Stage stage = (Stage) player1.getScene().getWindow();
        Scene scene = new Scene(parent);
        resultService.setReady(false);
        questionService.setReady(false);
        stage.setScene(scene);
        stage.show();
    }

    private void sendReq() throws JsonProcessingException {
        QuestionRequestDto questionRequestDto = questionService.prepareQuestReq();
        Request<QuestionRequestDto> request = new Request<>();
        request.setData(questionRequestDto);
        request.setHeader("quesReq");
        get().getClient().sendMessage(mapper.writeValueAsString(request));
        System.out.println(this.getClass().getSimpleName() + " request sent");
    }

    private class Timer extends AnimationTimer {
        @Override
        public void handle(long now) {
            opacity.set(opacity.get() - 1.5);
            if (opacity.get() <= 0) {
                timeIsUp = true;
                questionWaiter.start();
                stop();
            }
        }
    }

    private class QuestionWaiter extends Thread {
        private ResultsController resultsController;
        private volatile boolean ready;

        public QuestionWaiter(ResultsController resultsController) {
            this.resultsController = resultsController;
        }

        @SneakyThrows
        @Override
        public void run() {
            while (!questionService.isReady()) {

            }
            ready = true;
            questionService.setReady(false);
            Runnable runnable = () ->
            {
                try {
                    resultsController.redirect();

                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
            };

            if (ready && cont && Platform.isFxApplicationThread() && timeIsUp) {
                new Thread(runnable).start();
            } else if (!Platform.isFxApplicationThread()) Platform.runLater(runnable);
        }
    }
}
