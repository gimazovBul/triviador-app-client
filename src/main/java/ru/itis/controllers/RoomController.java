package ru.itis.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;
import ru.itis.client.Client;
import ru.itis.clientConfig.ClientContext;
import ru.itis.clientServices.interfaces.QuestionService;
import ru.itis.clientServices.interfaces.RoomService;
import ru.itis.clientSideWrappers.RoomWrapper;
import ru.itis.clientSideWrappers.UserWrapper;
import ru.itis.dto.QuestionRequestDto;
import ru.itis.dto.RoomDto;
import ru.itis.dto.UserDto;
import ru.itis.protocol.Request;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;


public class RoomController implements Initializable {
    @FXML
    public Button start;
    @FXML
    private Label player1;
    @FXML
    private Label player2;
    @FXML
    private Label player3;
    @FXML
    private Label ready1;
    @FXML
    private Label ready2;
    @FXML
    private Label ready3;

    @FXML
    private Label playerDiscr;

    List<Label> playersLabels;
    List<Label> readyLabels;

    private RoomService roomService;
    private QuestionService questionService;
    private ClientContext clientContext;
    private Client client;
    private ObjectMapper mapper;
    private QuestionWaiter questionWaiter;

    public void showRoom() {
        RoomDto roomDto = roomService.getRoom();
        if (roomDto.getPlayers().get(0) != null)
            player1.setTextFill(Paint.valueOf(roomDto.getPlayers().get(0).getColor()));
        ready1.setVisible(true);
        if (roomDto.getPlayers().get(1) != null)
            player2.setTextFill(Paint.valueOf(roomDto.getPlayers().get(1).getColor()));
        ready2.setVisible(true);
        if (roomDto.getPlayers().get(2) != null)
            player3.setTextFill(Paint.valueOf(roomDto.getPlayers().get(2).getColor()));
        ready3.setVisible(true);
        for (UserDto u : roomDto.getPlayers()) {
            if (u.getId().equals(UserWrapper.get().getId())) {
                UserWrapper.set(u);
                playerDiscr.setText("You're Player" + (roomDto.getPlayers().lastIndexOf(u) + 1));
                playerDiscr.setTextFill(Paint.valueOf(u.getColor()));
                playerDiscr.setVisible(true);
                if (u.isHost()) {
                    start.setVisible(true);
                }
            }
        }
        RoomWrapper.set(roomDto);
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        System.out.println("initializing roomController");
        clientContext = ClientContext.get();
        roomService = clientContext.getRoomService();
        questionService = ClientContext.get().getQuestionService();
        client = clientContext.getClient();
        mapper = new ObjectMapper();
        questionWaiter = new QuestionWaiter(this);
        questionWaiter.setDaemon(true);
        questionWaiter.start();

        readyLabels = Arrays.asList(ready1, ready2, ready3);
        playersLabels = Arrays.asList(player1, player2, player3);
        showRoom();
    }

    public void redirect() throws IOException {
        System.out.println("redirecting to questionsController...");
        Parent parent = FXMLLoader.load(getClass().getResource("/fxml/questions.fxml"));
        Stage stage = (Stage) player1.getScene().getWindow();
        Scene scene = new Scene(parent);
        stage.setScene(scene);
        stage.show();
        if (!questionWaiter.isInterrupted()) questionWaiter.interrupt();
    }

    @FXML
    public void startGame() throws IOException {
        sendReq();
    }

    private void sendReq() throws JsonProcessingException {
        questionService = clientContext.getQuestionService();
        QuestionRequestDto questionRequestDto = questionService.prepareQuestReq();
        client = clientContext.getClient();
        Request<QuestionRequestDto> request = new Request<>();
        request.setData(questionRequestDto);
        request.setHeader("quesReq");
        client.sendMessage(mapper.writeValueAsString(request));
        System.out.println(this.getClass().getSimpleName() + " request sent");
    }

    private class QuestionWaiter extends Thread {
        private QuestionService questionService;
        private RoomController roomController;
        private volatile boolean ready;

        public QuestionWaiter(RoomController roomController) {
            this.roomController = roomController;
            ClientContext clientContext = ClientContext.get();
            questionService = clientContext.getQuestionService();
        }

        @Override
        public void run() {
            while (!questionService.isReady()) {

            }
            ready = true;
            questionService.setReady(false);
            Runnable runnable = () ->
            {
                try {
                    if (ready) {
                        roomController.redirect();
                    }
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
            };

            if (ready && Platform.isFxApplicationThread()) {
                new Thread(runnable).start();
            } else Platform.runLater(runnable);
        }

    }
}
