package ru.itis.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AnswerDto extends BaseDto {
   private String userId;

   //index of answer in array of question answers
   //-1 if player didn't answer
   private int answerInd;
}
