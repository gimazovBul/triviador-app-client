package ru.itis.dto;


import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

@Data
@NoArgsConstructor
public class BaseQuestionDto extends BaseDto {
    private String id;
    private int rightInd;
    private int max = 5;

    public BaseQuestionDto(int rightInd){
        this.rightInd = rightInd;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        BaseQuestionDto that = (BaseQuestionDto) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getId());
    }
}
