package ru.itis.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class CodeDto extends BaseDto{
    private String code;
    private String userId;

    public CodeDto(String code) {
        this.code = code;
    }
}