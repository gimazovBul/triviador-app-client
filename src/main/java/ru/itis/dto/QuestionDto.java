package ru.itis.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class QuestionDto extends BaseQuestionDto {

    public QuestionDto(int rightInd) {
        super(rightInd);
    }

    private String text;
    private int max = 2;
    private String[] answers;

}
