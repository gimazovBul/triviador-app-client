package ru.itis.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultDto extends BaseDto {
    private Type type;
    List<UserDto> users;

    public enum  Type {
        CONT, END
    }

}
